<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Register</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
      @csrf
      <h3>Sign Up Form</h3>
      <label>First Name:</label><br /><br />
      <input type="text" name="fname" /><br /><br />
      <label>Last Name:</label><br /><br />
      <input type="text" name="lname" /><br /><br />
      <label>Gender:</label><br /><br />
      <input type="radio" name="gender" />Male<br />
      <input type="radio" name="gender" />Female<br />
      <input type="radio" name="gender" />Other<br /><br />
      <label>Nationality:</label><br /><br />
      <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="singapore">Singapore</option>
        <option value="vietnam">Vietnam</option></select
      ><br /><br />
      <label>Language Spoken</label><br /><br />
      <input type="checkbox" name="language" />Bahasa Indonesia<br />
      <input type="checkbox" name="language" />English<br />
      <input type="checkbox" name="language" />Other<br /><br />
      <label>Bio:</label><br /><br />
      <textarea name="message" rows="10" cols="30"></textarea><br /><br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
