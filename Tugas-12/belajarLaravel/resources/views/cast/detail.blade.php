@extends('master')
@section('judul')
Halaman Tambah Pemain Film Baru
@endsection

@section('content')

<h1>{{$cast->nama}}_{{$cast->umur}}</h1>
<p>{{$cast->bio}}</p>
<a href="/cast" class="btn btn-secondary btn-sm">kembali</a>
@endsection