<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    
    public function regis (){
        return view('register');
    }
    public function welc (Request $request){
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
        return view('welcome' , ['namaDepan'=>$namaDepan,'namaBelakang'=>$namaBelakang]);
    }
}
