<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/register', [AuthController::class, 'regis']);
Route::post('/welcome', [AuthController::class, 'welc']);

Route::get('/table', function(){
    return view('table');
});

Route::get('/data-table', function(){
    return view('data-table');
});

// creat data
// form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// kirim/tambah data ke database
Route::post('/cast', [	CastController::class, 'store']);

// // read data
// // tampil data
Route::get('/cast', [CastController::class, 'index']);
// detail cast
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// update data
// form edit cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// form update data
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// delete data
// delete berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

