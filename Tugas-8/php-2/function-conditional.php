<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
function greetings($nama){
    echo "Halo " . $nama . ", selamat datang di Sanbercode!";
    echo "<br>";
}

greetings("Bagas");
greetings("Wahyu");
greetings("Aida");
echo "<br>";


echo "<h3>Soal No 2 Reverse String</h3>";
function reverse($kata1){
    $panjangKata = strlen($kata1);
    $tampung = "";
    for($i=($panjangKata -1);$i>=0;$i--){
        $tampung .= $kata1[$i];
    }
    return $tampung;
}

function reverseString($kata2){
    $string= reverse($kata2);
    echo $string . "<br>";
}
reverseString("Aida");
reverseString("Sanbercode");
reverseString("We Are Sanbers Developers");
echo "<br>";


echo "<h3>Soal No 3 Palindrome </h3>";
function palindrome($kata3)
{
$balik = reverse($kata3);
echo $kata3 == $balik ? $kata3 ." => true <br>" : $kata3 ." => false <br>";
}

palindrome("civic") ;
palindrome("nababan") ; 
palindrome("jambaban");
palindrome("racecar");


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
function tentukan_nilai($number){
    $output = "";
    if($number >=85 && $number <100){
        $output.="Sangat Baik";
    }else if($number >=70 && $number <85){
        $output.="Baik";
    }else if($number >=60 && $number <70){
        $output.="Cukup";
    }else if($number <60){
        $output.="Kurang";
    }
    return $output . "<br>";
}

echo tentukan_nilai(98);
echo tentukan_nilai(76);
echo tentukan_nilai(67); 
echo tentukan_nilai(43); 


?>

</body>

</html>