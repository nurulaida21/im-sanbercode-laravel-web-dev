<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Soal 1 </h3>";
   
    $kids=["Kd1"=>"Mike", "Kd2"=>"Dustin", "Kd3"=>"Will", "Kd4"=>"Lucas", "Kd5"=>"Max", "Kd6"=>"Eleven" ];
    print_r($kids);
    echo "<br><br>";   
    $adults=["Ad1"=>"Hopper", "Ad2"=>"Nancy",  "Ad3"=>"Joyce", "Ad4"=>"Jonathan", "Ad5"=>"Murray"];
    print_r($adults);



    echo "<h3> Soal 2</h3>";
    
    echo "Cast Stranger Things: ";
    echo "<br><br>";
    echo "Total Kids: ".count($kids); 
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[Kd1].</li>";
    echo "<li> $kids[Kd2].</li>";
    echo "<li> $kids[Kd3].</li>";
    echo "<li> $kids[Kd4].</li>";
    echo "<li> $kids[Kd5].</li>";
    echo "<li> $kids[Kd6].</li>";
    echo "</ol>";

    echo "Total Adults: ".count($adults);
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[Ad1] </li>";
    echo "<li> $adults[Ad2] </li>";
    echo "<li> $adults[Ad3] </li>";
    echo "<li> $adults[Ad4] </li>";
    echo "<li> $adults[Ad5] </li>";
    echo "</ol>";

    echo "<h3> Soal 3</h3>";
    $biodata=[
        ["Name:" => "Will Byers","Age:"=> 12,"Aliases:" => "Will the Wise","Status:"=>"Alive"],
        ["Name:" => "Mike Wheeler","Age:"=> 12,"Aliases:" => "Dungeon Master","Status:"=>"Alive"],
        ["Name:" => "Jim Hopper","Age:"=> 43,"Aliases:" => "Chief Hopper","Status:"=>"Deceased"],
        ["Name:" => "Eleven","Age:"=> 12,"Aliases:" => "El","Status:"=>"Alive"]
    ];
    echo"<pre>";
    print_r($biodata);
    echo"</pre>";
    ?>
</body>

</html>